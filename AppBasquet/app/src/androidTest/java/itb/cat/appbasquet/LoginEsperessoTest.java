package itb.cat.appbasquet;

import android.view.View;
import android.widget.EditText;

import androidx.annotation.StringRes;
import androidx.test.espresso.ViewAssertion;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import itb.cat.appbasquet.screen.login.LoginFragment;

import static android.provider.Settings.System.getString;
import static android.service.autofill.Validators.not;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static io.grpc.LoadBalancer.PickResult.withError;
import static java.util.regex.Pattern.matches;

@RunWith(AndroidJUnit4.class)
public class LoginEsperessoTest {

   /** @Rule
    public ActivityTestRule<LoginFragment> activityTestRule = new ActivityTestRule<>(LoginFragment.class);

    @Test
    public void emailIsEmpty(){
        onView(withId(R.id.userName)).perform(clearText());
        onView(withId(R.id.loginButton)).perform(click());
        onView(withId(R.id.userName)).check(matches(withError(getString(R.string.fieldRequired))));
    }

    @Test
    public void passwordIsEmpty() {
        onView(withId(R.id.userName)).perform(typeText("email@email.com"), closeSoftKeyboard());
        onView(withId(R.id.userPassword)).perform(clearText());
        onView(withId(R.id.loginButton)).perform(click());
        onView(withId(R.id.userPassword)).check(matches(withError(getString(R.string.fieldRequired))));
    }

    @Test
    public void emailIsInvalid() {
        onView(withId(R.id.userName)).perform(typeText("invalid"), closeSoftKeyboard());
        onView(withId(R.id.loginButton)).perform(click());
        //onView(withId(R.id.userName)).check(matches(withError(getString(R.string.fieldRequired))));
    }

    @Test
    public void passwordIsTooShort() {
        onView(withId(R.id.userName)).perform(typeText("email@email.com"), closeSoftKeyboard());
        onView(withId(R.id.userPassword)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.loginButton)).perform(click());
        //onView(withId(R.id.userPassword)).check(matches(withError(getString(R.string.fieldRequired))));
    }


    private String getString(@StringRes int resourceId) {
        return activityTestRule.getActivity().getString(resourceId);
    }

    private static Matcher<View> withError(final String expected) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                if (item instanceof EditText) {
                    return ((EditText)item).getError().toString().equals(expected);
                }
                return false;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Not found error message" + expected + ", find it!");
            }
        };
    }*/
}
