package itb.cat.appbasquet.screen.profile;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.appbasquet.R;

public class ProfileViewFragment extends Fragment {

    private ProfileViewViewModel mViewModel;

    public static ProfileViewFragment newInstance() {
        return new ProfileViewFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_view_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.changeProfileBtn, R.id.resetPasswordBtn, R.id.logoutBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.changeProfileBtn:
                navigate(R.id.goToEditProfile);
                break;
            case R.id.resetPasswordBtn:
                navigate(R.id.fogotPassword1);
                break;
            case R.id.logoutBtn:
                navigate(R.id.goTologgedOut);
                break;

        }
    }

    private void navigate(int option) {
        Navigation.findNavController(getView()).navigate(option);

    }

}
