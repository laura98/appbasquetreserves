package itb.cat.appbasquet.repository;

import java.util.ArrayList;
import java.util.List;

import itb.cat.appbasquet.model.Reservation;

import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class BasquetRepository {
    private DatabaseReference basquetDbReference;
    Reservation basquetReservation;
    private static final int COST_PER_UNIT = 10;
    private static final double IVA = 1.21;

    public void addReservation() {
        basquetDbReference = FirebaseDatabase.getInstance().getReference().child("Reservation");

        basquetReservation = new Reservation();
        basquetReservation.setId(1);
        basquetReservation.setUserName("Laura");
        basquetReservation.setAlleyNumber(3);
        List<String> players = new ArrayList<>();
        players.add("Lau");
        players.add("Luis");
        players.add("Albert");
        players.add("Joan");
        basquetReservation.setPlayers(players);
        basquetReservation.setTime("17:00-18:30");

        double reservationCost = Math.round((players.size()*COST_PER_UNIT*IVA) * 100.0) / 100.0;

        basquetReservation.setReservationCost(reservationCost);

        basquetDbReference.push().setValue(basquetReservation);

    }
}
