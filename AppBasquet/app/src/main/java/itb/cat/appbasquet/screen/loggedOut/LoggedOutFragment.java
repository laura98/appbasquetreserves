package itb.cat.appbasquet.screen.loggedOut;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import itb.cat.appbasquet.R;

public class LoggedOutFragment extends Fragment {

    private LoggedOutViewModel mViewModel;

    public static LoggedOutFragment newInstance() {
        return new LoggedOutFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logged_out_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoggedOutViewModel.class);
        // TODO: Use the ViewModel
    }

}
