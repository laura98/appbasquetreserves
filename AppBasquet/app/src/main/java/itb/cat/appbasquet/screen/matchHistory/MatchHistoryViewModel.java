package itb.cat.appbasquet.screen.matchHistory;

import android.widget.TextView;

import androidx.lifecycle.ViewModel;

public class MatchHistoryViewModel extends ViewModel {

    private int punts;

    public int getPunts(){
        return punts;
    }

    public void setPunts(int p){
        this.punts = p;
    }

    public void sumarPunts(int i) {
        punts+= i;
    }

    public void reset() {
        punts = 0;
    }
}
