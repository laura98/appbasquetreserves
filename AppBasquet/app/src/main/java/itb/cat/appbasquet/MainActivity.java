package itb.cat.appbasquet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import itb.cat.appbasquet.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
