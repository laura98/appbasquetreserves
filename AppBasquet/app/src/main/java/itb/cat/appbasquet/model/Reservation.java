package itb.cat.appbasquet.model;

import com.google.firebase.database.PropertyName;

import java.util.List;

public class Reservation {

    private Integer id;
    private String userName;
    private Integer alleyNumber;
    private String time;
    private List<String> players;
    private double reservationCost;



    public Reservation() {
    }

    @PropertyName("Reservation ID")
    public Integer getId() {
        return id;
    }

    @PropertyName("Reservation ID")
    public void setId(Integer id) {
        this.id = id;
    }

    @PropertyName("User Name")
    public String getUserName() {
        return userName;
    }

    @PropertyName("User Name")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @PropertyName("Alley Number")
    public Integer getAlleyNumber() {
        return alleyNumber;
    }

    @PropertyName("Alley Number")
    public void setAlleyNumber(Integer alleyNumber) {
        this.alleyNumber = alleyNumber;
    }

    @PropertyName("Time")
    public String getTime() {
        return time;
    }

    @PropertyName("Time")
    public void setTime(String time) {
        this.time = time;
    }

    @PropertyName("Players")
    public List<String> getPlayers() {
        return players;
    }

    @PropertyName("Players")
    public void setPlayers(List<String> players) {
        this.players = players;
    }

    @PropertyName("Total cost")
    public double getReservationCost() {
        return reservationCost;
    }
    @PropertyName("Total cost")
    public void setReservationCost(double reservationCost) {
        this.reservationCost = reservationCost;
    }
}
