package itb.cat.appbasquet.screen.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.appbasquet.R;

public class MainMenuFragment extends Fragment {

    @BindView(R.id.reservations)
    LinearLayout reservations;
    @BindView(R.id.offers)
    LinearLayout offers;
    @BindView(R.id.friends)
    LinearLayout friends;
    @BindView(R.id.match_history)
    LinearLayout matchHistory;
    @BindView(R.id.profile)
    LinearLayout profile;
    @BindView(R.id.contact)
    LinearLayout contact;
    @BindView(R.id.bugs)
    LinearLayout bugs;
    @BindView(R.id.help)
    LinearLayout help;
    @BindView(R.id.menuScrollID)
    ScrollView menuScrollID;
    private MainMenuViewModel mViewModel;

    public static MainMenuFragment newInstance() {
        return new MainMenuFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_menu_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainMenuViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    private void navigate(int option) {
        Navigation.findNavController(getView()).navigate(option);

    }

    @OnClick({R.id.reservations, R.id.offers, R.id.friends, R.id.match_history, R.id.profile, R.id.contact, R.id.bugs, R.id.help})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.reservations:
                navigate(R.id.goToReservations);
                break;
            case R.id.offers:
                navigate(R.id.goToOffers);
                break;
            case R.id.friends:
                navigate(R.id.goToFriends);
                break;
            case R.id.match_history:
                navigate(R.id.goToMatchHistory);
                break;
            case R.id.profile:
                navigate(R.id.goToProfile);
                break;
            case R.id.contact:
                navigate(R.id.goToContactUS);
                break;
            case R.id.bugs:
                navigate(R.id.goToReportBug);
                break;
            case R.id.help:
                navigate(R.id.goToHelp);
                break;
        }
    }
}
