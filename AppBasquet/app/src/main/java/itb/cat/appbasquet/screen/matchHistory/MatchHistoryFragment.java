package itb.cat.appbasquet.screen.matchHistory;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.appbasquet.R;

public class MatchHistoryFragment extends Fragment {

    @BindView(R.id.sumar1)
    Button sumar1Btn;
    @BindView(R.id.sumar2)
    Button sumar2Btn;
    @BindView(R.id.sumar3)
    Button sumar3Btn;
    @BindView(R.id.puntsTotal)
    Button puntsTotal;
    @BindView(R.id.textPunts)
    TextView puntsTextView;
    @BindView(R.id.finish)
    TextView finishMatch;

    private MatchHistoryViewModel mViewModel;

    public static MatchHistoryFragment newInstance() {
        return new MatchHistoryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.match_history_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MatchHistoryViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.sumar1, R.id.sumar2, R.id.sumar3, R.id.puntsTotal, R.id.finish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sumar1:
                sumar1();
                break;
            case R.id.sumar2:
                sumar2();
                break;
            case R.id.sumar3:
                sumar3();
                break;
            case R.id.finish:
                finishMatchBtn();
                break;

        }
    }
    private void sumar1(){
        mViewModel.sumarPunts(1);
    }

    private void sumar2 () {
        mViewModel.sumarPunts(2);
    }

    private void sumar3 () {
        mViewModel.sumarPunts(3);
    }

    private void finishMatchBtn () {
        puntsTextView.setText(String.valueOf(mViewModel.getPunts()));
        mViewModel.reset();
    }

}
