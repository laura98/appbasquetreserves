package itb.cat.appbasquet.screen.register;

import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import itb.cat.appbasquet.MainActivity;
import itb.cat.appbasquet.R;
import itb.cat.appbasquet.screen.login.LoginFragment;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class RegistrerFragment extends Fragment {

    private RegistrerViewModel mViewModel;
    private TextInputLayout usernameText2Layout, passwordText2Layout, emailTextLayout, phoneTextLayout;

    FirebaseAuth fAuth;
    ProgressDialog dialog;

    public static RegistrerFragment newInstance(){
        return new RegistrerFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate( R.layout.registrer_fragment, container, false );
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //usernameText2Layout = view.findViewById(R.id.editText2 );
        //passwordText2Layout = view.findViewById(R.id.editText5 );
        //emailTextLayout = view.findViewById(R.id.editText3);
        //phoneTextLayout = view.findViewById(R.id.editText7);

        //Button buttonLogin3 = view.findViewById(R.id.loginFromRegister);
        //buttonLogin3.setOnClickListener(this::Login);
        //Button buttonRegister3 = view.findViewById(R.id.registerButton);
        //buttonRegister3.setOnClickListener(this::Register);

        fAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );
        mViewModel = ViewModelProviders.of( this ).get( RegistrerViewModel.class );


    }
    private void loadingObserver(Boolean bool) {
        if (bool) {
            dialog = ProgressDialog.show(getContext(), "Loading", "...", true);
            dialog.show();
        } else {
            if(dialog!=null)
                dialog.dismiss();
        }
    }

    private void loggedObserve(Boolean logged) {
        if (logged) {
            Navigation.findNavController(getView()).navigate(R.id.goToMainMenuFromRegister);
        }
    }

    private void errorObserve(String error) {
        if (error!=null) {
            Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }

    private void Login(View view) {

        Navigation.findNavController(view).navigate(R.id.goToLoginFromRegister);
    }

    private void Register(View view) {


            String email = emailTextLayout.toString().trim();
            String password = passwordText2Layout.toString().trim();

            fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getContext(), "User Created.", Toast.LENGTH_SHORT).show();

                        Navigation.findNavController( view ).navigate(R.id.goToLoginFromRegister);
                        //startActivity(new Intent(getContext(),LoginFragment.class));

                    }else{
                        Toast.makeText(getContext(),"Error!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
    }

    private boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    private void scrollTo (TextInputLayout targetView){
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

}
