package itb.cat.appbasquet.screen.reservations;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.appbasquet.R;

public class MakeOrCancelReservationFragment extends Fragment {

    private MakeOrCancelReservationViewModel mViewModel;

    public static MakeOrCancelReservationFragment newInstance() {
        return new MakeOrCancelReservationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.make_or_cancel_reservation_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MakeOrCancelReservationViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.makeResevation, R.id.cancelReservation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.makeResevation:
                navigate(R.id.goToMakeReservation);
                break;
            case R.id.cancelReservation:
                //navigate(R.id.goToMainMenuAfterCancel);
                break;
        }
    }

    private void navigate(int option) {
        Navigation.findNavController(getView()).navigate(option);

    }


}
