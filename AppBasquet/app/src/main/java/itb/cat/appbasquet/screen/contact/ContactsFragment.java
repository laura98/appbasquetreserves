package itb.cat.appbasquet.screen.contact;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.appbasquet.R;

public class ContactsFragment extends Fragment {

    private ContactsViewModel mViewModel;

    public static ContactsFragment newInstance() {
        return new ContactsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contacts_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ContactsViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick({R.id.addContact, R.id.contactsView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addContact:
                navigate(R.id.goToAddContact);
                break;
            case R.id.contactsView:
                navigate(R.id.goToViewFragment);
                break;
        }
    }


    private void navigate(int option) {
        Navigation.findNavController(getView()).navigate(option);

    }

}
