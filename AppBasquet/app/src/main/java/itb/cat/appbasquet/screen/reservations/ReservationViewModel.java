package itb.cat.appbasquet.screen.reservations;

import androidx.lifecycle.ViewModel;

import itb.cat.appbasquet.repository.BasquetRepository;

public class ReservationViewModel extends ViewModel {
    private BasquetRepository bowlingRepository;

    public void addReservation() {
        if(bowlingRepository==null) {
            bowlingRepository = new BasquetRepository();
        }

        bowlingRepository.addReservation();

    }
}
