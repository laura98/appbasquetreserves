package itb.cat.appbasquet.screen.login;

import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.appbasquet.MainActivity;
import itb.cat.appbasquet.R;
import itb.cat.appbasquet.screen.register.RegistrerFragment;

import static android.widget.Toast.LENGTH_SHORT;

public class LoginFragment extends Fragment {

    @BindView(R.id.appIcon)
    ImageView appIcon;
    @BindView(R.id.userName)
    EditText userName;
    @BindView(R.id.userPassword)
    EditText userPassword;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.textOr)
    TextView textOr;
    @BindView(R.id.textSignIn)
    TextView textSignIn;
    @BindView(R.id.googleButton)
    ImageButton googleButton;
    @BindView(R.id.facebookButton)
    ImageButton facebookButton;
    @BindView(R.id.twitterButton)
    ImageButton twitterButton;
    //@BindView(R.id.textViewRegister)
    //TextView registerBtn;
    //@BindView(R.id.forgotPassword)
    //TextView forgotPassword;

    TextView forgotPasswordTextLink;

    private LoginViewModel mViewModel;

    FirebaseAuth mAuth;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
    }

    @OnClick({R.id.loginButton, R.id.googleButton, R.id.facebookButton, R.id.twitterButton, R.id.textViewRegister, R.id.forgotPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                navigateToMainMenu();
                break;
            case R.id.googleButton:
                signInWithGoogle();
                break;
            case R.id.facebookButton:
                signInWithFacebook();
                break;
            case R.id.twitterButton:
                signInWithTwitter();
                break;
            case R.id.textViewRegister:
                goRegister();
                break;
            case R.id.forgotPassword:
                /**forgotPasswordTextLink = view.findViewById(R.id.textInputLayoutForgotPassword);
                forgotPasswordTextLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText resetMail = new EditText(v.getContext());
                        AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());
                        passwordResetDialog.setTitle("Reset Password ?");
                        passwordResetDialog.setMessage("Enter Your Email To Received Reset Link?");
                        passwordResetDialog.setView(resetMail);

                        passwordResetDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String mail = resetMail.getText().toString();
                                mAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        //Toast.makeText(LoginFragment.this, "Reset link Sent To Your Email", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //Toast.makeText(LoginFragment.this, "Error! Reset Link is Not Sent", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                        passwordResetDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        passwordResetDialog.create().show();
                    }
                });*/
                goForgotPassword();
                break;
        }
    }

    private void goForgotPassword() {
        Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_forgotPasswordFragment);
    }

    private void goRegister() {
        Navigation.findNavController(getView()).navigate(R.id.goToRegister);
    }

    private void signInWithTwitter() {
    }

    private void signInWithFacebook() {
    }

    private void signInWithGoogle() {

    }

    private void navigateToMainMenu() {
        //NavDirections action = LoginFragmentDirections.goToMainMenu();
        String email = userName.getText().toString().trim();
        String password = userPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            userName.setError("Email is Required");
            return;
        }

        if(TextUtils.isEmpty(password)){
            userPassword.setError("Password is Required");
            return;
        }


        /**mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                                                        @Override
                                                                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                                                                            if (task.isSuccessful()) {
                                                                                                Toast.makeText(LoginFragment.this, "Logged in SuccessFully.", Toast.LENGTH_SHORT).show();
                                                                                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                                                                            } else {
                                                                                                Toast.makeText(LoginFragment.this, "Error! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                        }
                                                                                    });*/
        Navigation.findNavController(getView()).navigate(R.id.goToMainMenu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        /**registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        /**forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText resetMail = new EditText(v.getContext());
                AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());
                passwordResetDialog.setTitle("Reset Password?");
                passwordResetDialog.setMessage("Enter your Email To Received Reset Link.");
                passwordResetDialog.setView(resetMail);

                passwordResetDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //extract the email and send reset link
                        String mail = resetMail.getText().toString();
                        mAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                //Toast.makeText(LoginFragment.this, "Reset Link Sent To Your Email", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                //Toast.makeText(LoginFragment.this, "Error! Reset Link is Not Sent" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                passwordResetDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //close the dialoge
                    }
                });
            }
        });*/

    }
}
